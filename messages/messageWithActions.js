const stride = require("../strideClient");
const { Document, Marks } = require("adf-builder");

/*
  Dialog
  Config
  Sidebar
  External
  Service
 */

function createCard() {
  let doc = new Document();
  doc.heading(2).text("Actions in Cards");

  const parameters = { server: `side-${new Date().getTime()}` };
  let card1 = doc.applicationCard("Actions in Cards - 1");
  let card2 = doc.applicationCard("Actions in Cards - 2");
  let card3 = doc.applicationCard("Actions in Cards - 3");

  //Dialog button
  card1
    .action()
    .title("Dialog")
    .target({ key: "actionTarget-openDialog" })
    .parameters(parameters);

  //Configuration button
  card1
    .action()
    .title("Config")
    .target({ key: "actionTarget-openConfiguration" })
    .parameters(parameters);

  //Sidebar button
  card1
    .action()
    .title("Sidebar")
    .target({ key: "actionTarget-openSidebar" })
    .parameters(parameters);

  //External
  card2
    .action()
    .title("External")
    .target({ key: "actionTarget-openExternalPage" })
    .parameters(parameters);

  card2
    .action()
    .title("Service")
    .target({ key: "actionTarget-callService-success" })
    .parameters(parameters);

  //Fail button
  card3
    .action()
    .title("Call Service and fail")
    .target({ key: "actionTarget-callService-failure" })
    .parameters(parameters);

  //Next Action button
  card3
    .action()
    .title("Call Service and Open dialog")
    .target({ key: "actionTarget-callService-nextAction" })
    .parameters(parameters);

  return { body: doc.toJSON() };
}

function createInlineActionButtons() {
  let doc = {
    version: 1,
    type: "doc",
    content: [
      {
        attrs: {
          level: 2
        },
        content: [
          {
            text: "Inline Actions as buttons in messages",
            type: "text"
          }
        ],
        type: "heading"
      },
      {
        type: "paragraph",
        content: [
          { type: "text", text: "Or create them as inline buttons : " },
          {
            type: "inlineExtension",
            attrs: {
              extensionType: "com.atlassian.stride",
              extensionKey: "actionGroup",
              parameters: {
                key: "inline-action-group-key",
                actions: [
                  {
                    key: "openDialog",
                    title: "Open Dialog",
                    appearance: "primary",
                    action: {
                      target: {
                        key: "actionTarget-openDialog"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "openConfig",
                    title: "Open Config",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-openConfiguration"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "openSidebar",
                    title: "Open Sidebar",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-openSidebar"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "openExternal",
                    title: "Open External",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-openExternalPage"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "callService-success",
                    title: "Call Service",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-callService-success"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "callService-failure",
                    title: "Call Service and fail",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-callService-failure"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  },
                  {
                    key: "callService-nextAction",
                    title: "Call Service and Open dialog",
                    appearance: "default",
                    action: {
                      target: {
                        key: "actionTarget-callService-nextAction"
                      },
                      parameters: {
                        message: "buttons"
                      }
                    }
                  }
                ]
              }
            }
          }
        ]
      }
    ]
  };
  return { body: doc };
}

function createInlineSelectMessage() {
  const doc = {
    version: 1,
    type: "doc",
    content: [
      {
        attrs: {
          level: 2
        },
        content: [
          {
            text: "Inline select actions",
            type: "text"
          }
        ],
        type: "heading"
      },
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "What should I do next?",
            marks: [
              {
                type: "strong"
              }
            ]
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "Call a service exposed by the app, then execute another action client-side:"
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "inlineExtension",
            attrs: {
              extensionType: "com.atlassian.stride",
              extensionKey: "select",
              parameters: {
                key: "inlineSelect-nextAction",
                title: "Choose next action",
                source: "custom",
                data: {
                  value: "",
                  options: [
                    {
                      value: "openSidebar",
                      title: "Open a sidebar"
                    },
                    {
                      value: "openDialog",
                      title: "Open a dialog"
                    },
                    {
                      value: "openHighlights",
                      title: "Open room highlights"
                    },
                    {
                      value: "openRoom",
                      title: "Create and open a room"
                    },
                    {
                      value: "openFiles",
                      title: "Open room files and links"
                    },
                    {
                      value: "error",
                      title: "Hit service and error"
                    }
                  ]
                },
                action: {
                  target: {
                    key: "actionTarget-handleInlineMessageSelect"
                  }
                }
              }
            }
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "Open a dialog, passing the following custom parameter:"
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "inlineExtension",
            attrs: {
              extensionType: "com.atlassian.stride",
              extensionKey: "select",
              parameters: {
                key: "inlineSelect-sendToDialog",
                title: "Choose parameter value",
                source: "custom",
                data: {
                  value: "",
                  options: [
                    {
                      value: "value1",
                      title: "Value 1"
                    },
                    {
                      value: "value2",
                      title: "Value 2"
                    }
                  ]
                },
                action: {
                  target: {
                    key: "actionTarget-openDialog"
                  }
                }
              }
            }
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "Open a dialog, passing the selected user:"
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "inlineExtension",
            attrs: {
              extensionType: "com.atlassian.stride",
              extensionKey: "select",
              parameters: {
                key: "inlineSelect-sendUserToDialog",
                title: "Start typing a user name",
                source: "users_in_site",
                action: {
                  target: {
                    key: "actionTarget-openDialog"
                  }
                }
              }
            }
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "Open a dialog, passing the selected room:"
          }
        ]
      },
      {
        type: "paragraph",
        content: [
          {
            type: "inlineExtension",
            attrs: {
              extensionType: "com.atlassian.stride",
              extensionKey: "select",
              parameters: {
                key: "inlineSelect-sendRoomToDialog",
                title: "Start typing a room name",
                source: "rooms_in_site",
                action: {
                  target: {
                    key: "actionTarget-openDialog"
                  }
                }
              }
            }
          }
        ]
      }
    ]
  };

  return { body: doc };
}

function createInlineActionLinks() {
  let doc = new Document();
  doc.heading(2).text("Inline Actions as links in messages");

  doc.paragraph().text("Create link actions using ADF Marks on text nodes.");
  let bulletList = doc.bulletList();

  const parameters = { server: `side-${new Date().getTime()}` };

  bulletList
    .textItem("Open Dialog", new Marks().action("Open Dialog", { key: "actionTarget-openDialog" }, parameters))
    .textItem("Open Config", new Marks().action("Open Config", { key: "actionTarget-openConfiguration" }, parameters))
    .textItem("Open Sidebar", new Marks().action("Open Sidebar", { key: "actionTarget-openSidebar" }, parameters))
    .textItem(
      "Open External Page",
      new Marks().action("Open External Page", { key: "actionTarget-openExternalPage" }, parameters)
    )
    .textItem(
      "Call Service",
      new Marks().action("Call Service", { key: "actionTarget-callService-success" }, parameters)
    )
    .textItem(
      "Call Service and Open dialog",
      new Marks().action("Call Service and Open dialog", { key: "actionTarget-callService-nextAction" }, parameters)
    );

  return { body: doc.toJSON() };
}

//Returns a public room the app is installed in that isn't the current conversation
//Creates a new one if there aren't any others
async function getRoomForMessage(cloudId, conversationId){
  let conversations = await stride.api.conversations.getAll(cloudId, {qs: { 'exclude-direct':'true', 'include-private':'false', 'include-archived':'true' }});
  let [conversation] = conversations.values.filter(item => item.id !== conversationId); //Take the first from the list, undefined if empty
  if(!conversation){
    let opts = {
      body: {
        name: "Stride - Tour of Actions test room" + new Date().getTime(),
        privacy: "public",
        topic: "This room was created via the API"
      }
    };

    conversation = await stride.api.conversations.create(cloudId, opts);
  }
  return conversation;
}


async function specialActions(cloudId, conversationId) {
  let doc = new Document();
  let conversation = await getRoomForMessage(cloudId, conversationId);

  const parameters = { server: `side-${new Date().getTime()}` };
  doc.heading(2).text("Special Actions ");

  doc
    .paragraph()
    .text(
      "You can also create special actions that control some elements of the GUI. " +
        "You can open any Conversation using a conversationId, the Files and Links sidebar, or the Highlights sidebar. "
    )
    .text(
      "Documentation",
      new Marks().link("https://developer.atlassian.com/cloud/stride/learning/adding-actions/#built-in-action-targets")
    );
  let bulletList = doc.bulletList();

  bulletList
    .textItem(
      "Open Files and Links",
      new Marks().action("Open Dialog", { key: "openFilesAndLinks", receiver: "stride" }, parameters)
    )
    .textItem(
      "Open Highlights",
      new Marks().action("Open Config", { key: "openHighlights", receiver: "stride" }, parameters)
    )
    .textItem(
      "Open a Conversation",
      new Marks().action(
        "Open Sidebar",
        { key: "openConversation", receiver: "stride" },
        { conversationId: conversation.id }
      )
    );

  return { body: doc.toJSON() };
}

module.exports = async function(cloudId, conversationId) {
  //send responses to the conversation
  await stride.api.messages.sendMessage(cloudId, conversationId, createCard());
  await stride.api.messages.sendMessage(cloudId, conversationId, createInlineActionLinks());
  await stride.api.messages.sendMessage(cloudId, conversationId, createInlineActionButtons());
  await stride.api.messages.sendMessage(cloudId, conversationId, await specialActions(cloudId, conversationId));
  await stride.api.messages.sendMessage(cloudId, conversationId, createInlineSelectMessage());
};
